# Miscellaneous Scripts

Hey! As a hobby, I like to tinker with the interfaces of Unix systems. Some of them aren't documented well. <br>
Here's some of the docs and scripts that I've created in order to help myself. Maybe they can help you too. :) <br>

[X11 Cursors](x11-cursors/x11-cursors-documentation.md)