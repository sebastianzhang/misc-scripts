# X11 Cursor Cheatsheet

Here's an easy cheatsheet for making cursor themes on X11. <br>
Just draw one of each of these, and symlink the extra names to the original. <br>
This should cover everything that Adawaita covers. There are 62 unique cursors. <br>
For more detailed documentation on where each name commonly shows up, see the [full documentation](x11-cursors-documentation.md).

## Cursors

### Default Cursor
![default](images/default.png) `default` - `arrow` - `left_ptr` - `top_left_arrow`

### Wait/Loading Cursor
![progress](images/progress.png) - `progress` - `left_ptr_watch` - `08e8e1c95fe2fc01f976f1e063a24ccd` - `3ecb610c1bf2410f44200f48c40d3599`

### Crossed Circle Cursor
![circle](images/circle.png) - `circle`

### Context Menu Cursor
![context-menu](images/context-menu.png) - `context-menu`

### Link Cursor
![link](images/link.png) - `link` - `3085a0e285430894940527032f8b26df` - `640fb0e74195791501fd1ed57b41487f`

### Move Cursor
![pointer-move](images/pointer-move.png) - `pointer-move`

### Right-Hand Cursor
![right_ptr](images/right_ptr.png) `right_ptr` - `draft_large` - `draft_small`

## General

### Crossed Circle
![crossed_circle](images/crossed_circle.png) - `crossed_circle` - `not-allowed` - `03b6e0fcb3499374a867c041f52298f0`

### Help
![help](images/help.png) - `help` - `left_ptr_help` - `question_arrow` - `5c6cd98b3f3ebcb1f9c7f1c204630408` - `d9ce0ab605698f320427677b458ad60b`

### Scrolling
![all-scroll](images/all-scroll.png) - `all-scroll`

### Text
![text](images/text.png) - `text` - `xterm` <br>
Personally, I make my `xterm` cursor the same as my `default` one.

### Vertical Text
![vertical-text](images/vertical-text.png) - `vertical-text`

### Wait/Loading
![wait](images/wait.png) - `wait` - `watch`

### Zoom-In
![zoom-in](images/zoom-in.png) - `zoom-in`

### Zoom-Out
![zoom-out](images/zoom-out.png) - `zoom-out`

## Hands

### Pointer
![hand](images/hand.png) - `hand` - `hand2` - `pointer` - `e29285e634086352946a0e7090d73106` - `9d800788f1b08800ae810202380a0822`

### Grabber Open
![grab](images/grab.png) - `grab` - `hand1` - `openhand`

### Grabber Closed
![grabbing](images/grabbing.png) - `grabbing`

### Grabber Closed No Icon
![dnd-none](images/dnd-none.png) - `dnd-none`

### Grabber Closed Link
![alias](images/alias.png) - `alias` - `dnd-link`

### Grabber Closed Help
![dnd-ask](images/dnd-ask.png) - `dnd-ask`

### Grabber Closed Copy
![copy](images/copy.png) - `copy` - `dnd-copy` - `1081e37283d90000800003c07f3ef6bf` - `6407b0e94181790501fd1e167b474872`

### Grabber Closed Move

![dnd-move](images/dnd-move.png) - `dnd-move` 

### Grabber Closed Crossed Circle

![no-drop](images/no-drop.png) - `no-drop` - `dnd-no-drop` 

## Resizing and Arrows

### Corners

#### Bottom Left Corner

![bottom_left_corner](images/bottom_left_corner.png) - `bottom_left_corner` - `sw-resize`

#### Bottom Right Corner

![bottom_right_corner](images/bottom_right_corner.png) - `bottom_right_corner` - `se-resize`

#### Top Left Corner

![top_left_corner](images/top_left_corner.png) - `top_left_corner` - `nw-resize`

#### Top Right Corner

![top_right_corner](images/top_right_corner.png) - `top_right_corner` - `ne-resize`

### Sides

#### Bottom Side

![bottom_side](images/bottom_side.png) - `bottom_side` - `s-resize`

#### Left Side

![left_side](images/left_side.png) - `left_side` - `w-resize`

#### Right Side

![right_side](images/right_side.png) - `right_side` - `e-resize`

#### Top Side

![top_side](images/top_side.png) - `top_side` - `n-resize`

### Single Arrows

#### Down Arrow

![sb_down_arrow](images/sb_down_arrow.png) - `sb_down_arrow`

#### Left Arrow

![sb_left_arrow](images/sb_left_arrow.png) - `sb_left_arrow`

#### Right Arrow

![sb_right_arrow](images/sb_right_arrow.png) - `sb_right_arrow`

#### Up Arrow

![sb_up_arrow](images/sb_up_arrow.png) - `sb_up_arrow`

### Double Arrows

#### Backward Diagonal Double Arrow

![bd_double_arrow](images/bd_double_arrow.png) - `bd_double_arrow` - `nwse-resize` - `size_fdiag` - `c7088f0f3e6c8088236ef8e1e3e70000`

#### Forward Diagonal Double Arrow

![fd_double_arrow](images/fd_double_arrow.png) - `fd_double_arrow` - `nesw-resize` - `size_bdiag` - `fcf1c3c7cd4491d801f1e1c78f100000`

#### Horizontal Double Arrow

![h_double_arrow](images/h_double_arrow.png) - `h_double_arrow` - `sb_h_double_arrow` - `col-resize` - `ew-resize` - `size_hor` - `028006030e0e7ebffc7f7070c0600140` - `14fef782d02440884392942c11205230`

#### Vertical Double Arrow

![double_arrow](images/double_arrow.png) - `double_arrow` - `ns-resize` - `row-resize` - `sb_v_double_arrow` - `size_ver` - `v_double_arrow` - `00008160000006810000408080010102` - `2870a09082c103050810ffdffffe0204`

### Quadruple Arrows

#### Move

![move](images/move.png) - `move` - `4498f0e0c1937ffe01fd06f973665830` - `9081237383d90e509aa00f00170e968f`

#### Size All

![size_all](images/size_all.png) - `size_all`

## Crosshairs

### Crosshair

![cross](images/cross.png) - `cross` - `cross_reverse` - `crosshair` - `diamond_cross`

### Text Crosshair

![tcross](images/tcross.png) - `tcross`

### Cell

![cell](images/cell.png) - `cell`

### Dot Box

![dotbox](images/dotbox.png) - `dotbox` - `dot_box_mask` - `draped_box`

### Plus

![plus](images/plus.png) - `plus`

## Tees

### Bottom Tee

![bottom_tee](images/bottom_tee.png) - `bottom_tee`

### Left Tee

![left_tee](images/left_tee.png) - `left_tee`

### Right Tee

![right_tee](images/right_tee.png) - `right_tee`

### Top Tee

![top_tee](images/top_tee.png) - `top_tee`

## Angles

### Lower Left Angle

![ll_angle](images/ll_angle.png) - `ll_angle`

### Lower Right Angle

![lr_angle](images/lr_angle.png) - `lr_angle`

### Upper Left Angle

![ul_angle](images/ul_angle.png) - `ul_angle`

### Upper Right Angle

![ur_angle](images/ur_angle.png) - `ur_angle`

## Rarely Used

### X Cursor

![X_cursor](images/X_cursor.png) - `X_cursor`

### Wayland Cursor

![wayland-cursor](images/wayland-cursor) - `wayland-cursor`

### Fleur

![fleur](images/fleur.png) - `fleur`

### Icon

![icon](images/icon.png) - `icon`

### Target

![target](images/target.png) - `target`

### Pencil

![pencil](images/pencil.png) - `pencil`

### Pirate

![pirate](images/pirate.png) - `pirate`