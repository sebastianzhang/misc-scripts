# X11 Cursor Documentation

Oh hey. <br>
I tried to make a cursor theme earlier, and it was a struggle to figure out what the correct names for each cursor should have been. <br>
Hopefully this resource is helpful for you. <br>
If you have any suggestions, please feel free to let me know. <br>
For an abridged version of this documentation (good for theme creators!), see the [cheatsheet](x11-cursors-cheatsheet.md).

# Naming Scheme
Each file within the cursors folder of a cursor theme must be named accordingly in order for it to work. <br>
There are probably more than are listed here, these are just the ones in Adawaita. <br>

Todo: add specific descriptions of where each cursor might appear. <br>

## Cursors

### Default Cursor

![default](images/default.png) - `default` - ??? <br>
![arrow](images/arrow.png) - `arrow` - ??? <br>
![left_ptr](images/left_ptr.png) - `left_ptr` - ??? <br>
![top_left_arrow](images/top_left_arrow.png) - `top_left_arrow` - ??? <br>

### Wait/Loading Cursor

![progress](images/progress.png) - `progress` - ??? <br>
![left_ptr_watch](images/left_ptr_watch.png) - `left_ptr_watch` - ??? <br>
![08e8e1c95fe2fc01f976f1e063a24ccd](images/08e8e1c95fe2fc01f976f1e063a24ccd.png) - `08e8e1c95fe2fc01f976f1e063a24ccd` - ??? <br>
![3ecb610c1bf2410f44200f48c40d3599](images/3ecb610c1bf2410f44200f48c40d3599.png) - `3ecb610c1bf2410f44200f48c40d3599` - ??? <br>

### Crossed Circle Cursor

![circle](images/circle.png) - `circle` - Not-allowed, but for your cursor. <br>

### Context Menu Cursor

![context-menu](images/context-menu.png) - `context-menu` - ??? <br>

### Link Cursor

![link](images/link.png) - `link` - ??? <br>
![3085a0e285430894940527032f8b26df](images/3085a0e285430894940527032f8b26df.png) - `3085a0e285430894940527032f8b26df` - ??? <br>
![640fb0e74195791501fd1ed57b41487f](images/640fb0e74195791501fd1ed57b41487f.png) - `640fb0e74195791501fd1ed57b41487f` - ??? <br>

### Move Cursor

![pointer-move](images/pointer-move.png) - `pointer-move` - ??? <br>

### Right-Hand Cursor

![draft_large](images/draft_large.png) - `draft_large` - ??? <br>
![draft_small](images/draft_small.png) - `draft_small` - ??? <br>
![right_ptr](images/right_ptr.png) - `right_ptr` - ??? <br>

TODO: I know there's a `center_ptr` or `middle_ptr`.

## General

### Crossed Circle

![crossed_circle](images/crossed_circle.png) - `crossed_circle` - ??? <br>
![not-allowed](images/not-allowed.png) - `not-allowed` - ??? <br>
![03b6e0fcb3499374a867c041f52298f0](images/03b6e0fcb3499374a867c041f52298f0.png) - `03b6e0fcb3499374a867c041f52298f0` - ??? <br>

### Help

![help](images/help.png) - `help` - ??? <br>
![left_ptr_help](images/left_ptr_help.png) - `left_ptr_help` - ??? <br>
![question_arrow](images/question_arrow.png) - `question_arrow` - ??? <br>
![5c6cd98b3f3ebcb1f9c7f1c204630408](images/5c6cd98b3f3ebcb1f9c7f1c204630408.png) - `5c6cd98b3f3ebcb1f9c7f1c204630408` - ??? <br>
![d9ce0ab605698f320427677b458ad60b](images/d9ce0ab605698f320427677b458ad60b.png) - `d9ce0ab605698f320427677b458ad60b` - ??? <br>

### Scrolling

![all-scroll](images/all-scroll.png) - `all-scroll` - ??? <br>

### Text

![text](images/text.png) - `text` - ??? <br>
![xterm](images/xterm.png) - `xterm` - Sets the cursor in xterm. <br>

### Vertical Text

![vertical-text](images/vertical-text.png) - `vertical-text` - ??? <br>

### Wait/Loading

![wait](images/wait.png) - `wait` - ??? <br>
![watch](images/watch.png) - `watch` - ??? <br>

### Zoom-In

![zoom-in](images/zoom-in.png) - `zoom-in` - ??? <br>

### Zoom-Out

![zoom-out](images/zoom-out.png) - `zoom-out` - ??? <br>

## Hands

### Pointer

![hand](images/hand.png) - `hand` - ??? <br>
![hand2](images/hand2.png) - `hand2` - ??? <br>
![pointer](images/pointer.png) - `pointer` - ??? <br>
![e29285e634086352946a0e7090d73106](images/e29285e634086352946a0e7090d73106.png) - `e29285e634086352946a0e7090d73106` - ??? <br>
![9d800788f1b08800ae810202380a0822](images/9d800788f1b08800ae810202380a0822.png) - `9d800788f1b08800ae810202380a0822` - ??? <br>

### Grabber Open

![grab](images/grab.png) - `grab` - ??? <br>
![hand1](images/hand1.png) - `hand1` - ??? <br>
![openhand](images/openhand.png) - `openhand` - ??? <br>

### Grabber Closed

![grabbing](images/grabbing.png) - `grabbing` - ??? <br>

### Grabber Closed No Icon

![dnd-none](images/dnd-none.png) - `dnd-none` - ??? <br>

### Grabber Closed Link

![alias](images/alias.png) - `alias` - ??? <br>
![dnd-link](images/dnd-link.png) - `dnd-link` - ??? <br>

### Grabber Closed Help

![dnd-ask](images/dnd-ask.png) - `dnd-ask` - ??? <br>

### Grabber Closed Copy

![copy](images/copy.png) - `copy` - ??? <br>
![dnd-copy](images/dnd-copy.png) - `dnd-copy` - ??? <br>
![1081e37283d90000800003c07f3ef6bf](images/1081e37283d90000800003c07f3ef6bf.png) - `1081e37283d90000800003c07f3ef6bf` - ??? <br>
![6407b0e94181790501fd1e167b474872](images/6407b0e94181790501fd1e167b474872.png) - `6407b0e94181790501fd1e167b474872` - ??? <br>

### Grabber Closed Move

![dnd-move](images/dnd-move.png) - `dnd-move` - ??? <br>

### Grabber Closed Crossed Circle

![no-drop](images/no-drop.png) - `no-drop` - ??? <br>
![dnd-no-drop](images/dnd-no-drop.png) - `dnd-no-drop` - ??? <br>

## Resizing and Arrows

### Corners

#### Bottom Left Corner

![bottom_left_corner](images/bottom_left_corner.png) - `bottom_left_corner` - ??? <br>
![sw-resize](images/sw-resize.png) - `sw-resize` - ??? <br>

#### Bottom Right Corner

![bottom_right_corner](images/bottom_right_corner.png) - `bottom_right_corner` - ??? <br>
![se-resize](images/se-resize.png) - `se-resize` - ??? <br>

#### Top Left Corner

![top_left_corner](images/top_left_corner.png) - `top_left_corner` - ??? <br>
![nw-resize](images/nw-resize.png) - `nw-resize` - ??? <br>

#### Top Right Corner

![top_right_corner](images/top_right_corner.png) - `top_right_corner` - ??? <br>
![ne-resize](images/ne-resize.png) - `ne-resize` - ??? <br>

### Sides

#### Bottom Side

![bottom_side](images/bottom_side.png) - `bottom_side` - ??? <br>
![s-resize](images/s-resize.png) - `s-resize` - ??? <br>

#### Left Side

![left_side](images/left_side.png) - `left_side` - ??? <br>
![w-resize](images/w-resize.png) - `w-resize` - ??? <br>

#### Right Side

![right_side](images/right_side.png) - `right_side` - ??? <br>
![e-resize](images/e-resize.png) - `e-resize` - ??? <br>

#### Top Side

![top_side](images/top_side.png) - `top_side` - ??? <br>
![n-resize](images/n-resize.png) - `n-resize` - ??? <br>

### Single Arrows

#### Down Arrow
![sb_down_arrow](images/sb_down_arrow.png) - `sb_down_arrow` - ??? <br>

#### Left Arrow
![sb_left_arrow](images/sb_left_arrow.png) - `sb_left_arrow` - ??? <br>

#### Right Arrow
![sb_right_arrow](images/sb_right_arrow.png) - `sb_right_arrow` - ??? <br>

#### Up Arrow
![sb_up_arrow](images/sb_up_arrow.png) - `sb_up_arrow` - ??? <br>

### Double Arrows

#### Backward Diagonal Double Arrow

![bd_double_arrow](images/bd_double_arrow.png) - `bd_double_arrow` - ??? <br>
![nwse-resize](images/nwse-resize.png) - `nwse-resize` - ??? <br>
![size_fdiag](images/size_fdiag.png) - `size_fdiag` - ??? <br>
![c7088f0f3e6c8088236ef8e1e3e70000](images/c7088f0f3e6c8088236ef8e1e3e70000.png) - `c7088f0f3e6c8088236ef8e1e3e70000` - ??? <br>

#### Forward Diagonal Double Arrow

![fd_double_arrow](images/fd_double_arrow.png) - `fd_double_arrow` - ??? <br>
![nesw-resize](images/nesw-resize.png) - `nesw-resize` - ??? <br>
![size_bdiag](images/size_bdiag.png) - `size_bdiag` - ??? <br>
![fcf1c3c7cd4491d801f1e1c78f100000](images/fcf1c3c7cd4491d801f1e1c78f100000.png) - `fcf1c3c7cd4491d801f1e1c78f100000` - ??? <br>

#### Horizontal Double Arrow

![h_double_arrow](images/h_double_arrow.png) - `h_double_arrow` - ??? <br>
![sb_h_double_arrow](images/sb_h_double_arrow.png) - `sb_h_double_arrow` - ??? <br>
![col-resize](images/col-resize.png) - `col-resize` - ??? <br>
![ew-resize](images/ew-resize.png) - `ew-resize` - ??? <br>
![size_hor](images/size_hor.png) - `size_hor` - ??? <br>
![028006030e0e7ebffc7f7070c0600140](images/028006030e0e7ebffc7f7070c0600140.png) - `028006030e0e7ebffc7f7070c0600140` - ??? <br>
![14fef782d02440884392942c11205230](images/14fef782d02440884392942c11205230.png) - `14fef782d02440884392942c11205230` - ??? <br>

#### Vertical Double Arrow

![double_arrow](images/double_arrow.png) - `double_arrow` - ??? <br>
![ns-resize](images/ns-resize.png) - `ns-resize` - ??? <br>
![row-resize](images/row-resize.png) - `row-resize` - ??? <br>
![sb_v_double_arrow](images/sb_v_double_arrow.png) - `sb_v_double_arrow` - ??? <br>
![size_ver](images/size_ver.png) - `size_ver` - ??? <br>
![v_double_arrow](images/v_double_arrow.png) - `v_double_arrow` - ??? <br>
![00008160000006810000408080010102](images/00008160000006810000408080010102.png) - `00008160000006810000408080010102` - ??? <br>
![2870a09082c103050810ffdffffe0204](images/2870a09082c103050810ffdffffe0204.png) - `2870a09082c103050810ffdffffe0204` - ??? <br>

### Quadruple Arrows

#### Move

![move](images/move.png) - `move` - ??? <br>
![4498f0e0c1937ffe01fd06f973665830](images/4498f0e0c1937ffe01fd06f973665830.png) - `4498f0e0c1937ffe01fd06f973665830` - ??? <br>
![9081237383d90e509aa00f00170e968f](images/9081237383d90e509aa00f00170e968f.png) - `9081237383d90e509aa00f00170e968f` - ??? <br>

#### Size All

![size_all](images/size_all.png) - `size_all` - ??? <br>

## Crosshairs

### Crosshair

![cross](images/cross.png) - `cross` - ??? <br>
![cross_reverse](images/cross_reverse.png) - `cross_reverse` - ??? <br>
![crosshair](images/crosshair.png) - `crosshair` - ??? <br>
![diamond_cross](images/diamond_cross.png) - `diamond_cross` - ??? <br>

### Text Crosshair

![tcross](images/tcross.png) - `tcross` - ??? <br>

### Cell

![cell](images/cell.png) - `cell` - Used in spreadsheet applications to select cells.  <br>

### Dot Box

![dotbox](images/dotbox.png) - `dotbox` - ??? <br>
![dot_box_mask](images/dot_box_mask.png) - `dot_box_mask` - ??? <br>
![draped_box](images/draped_box.png) - `draped_box` - ??? <br>

### Plus

![plus](images/plus.png) - `plus` - ??? <br>

## Tees

### Bottom Tee

![bottom_tee](images/bottom_tee.png) - `bottom_tee` - ??? <br>

### Left Tee

![left_tee](images/left_tee.png) - `left_tee` - ??? <br>

### Right Tee

![right_tee](images/right_tee.png) - `right_tee` - ??? <br>

### Top Tee

![top_tee](images/top_tee.png) - `top_tee` - ??? <br>

## Angles

### Lower Left Angle

![ll_angle](images/ll_angle.png) - `ll_angle` - ??? <br>

### Lower Right Angle

![lr_angle](images/lr_angle.png) - `lr_angle` - ??? <br>

### Upper Left Angle

![ul_angle](images/ul_angle.png) - `ul_angle` - ??? <br>

### Upper Right Angle

![ur_angle](images/ur_angle.png) - `ur_angle` - ??? <br>

## Rarely Used

### X Cursor

![X_cursor](images/X_cursor.png) - `X_cursor` - Cursor with the X logo on it. <br>

### Wayland Cursor

![wayland-cursor](images/wayland-cursor) - `wayland-cursor` - Cursor with the Wayland logo on it. <br>

### Fleur

![fleur](images/fleur.png) - `fleur` - ??? <br>

### Icon

![icon](images/icon.png) - `icon` - ??? <br>

### Target

![target](images/target.png) - `target` - ??? <br>

### Pencil

![pencil](images/pencil.png) - `pencil` - Used for drawing. <br>

### Pirate

![pirate](images/pirate.png) - `pirate` - Traditionally shown as a skull and crossbones. <br>

# "Woah! That's a lot of cursors! I wanna just draw and have fun though."

Fair. That's what I wanted too. <br>
I made a script that, if you name everything according to the first cursor of each "group" above, it will create symbolic links for the rest. <br>
It's in this directory. Have fun! <br>

# Theme Format

Format for cursors is `~/.local/share/icons/cursor_theme_name/cursors`, where `cursors` is the folder containing all of the X11 cursor files. <br>

# Installing

A lot of programs look in different places for cursors. <br>

## Xresources

In `~/.Xresources` add the following: <br>
```
Xcursor.theme: cursor_theme_name
```

## XDG Specification

Create `~/.icons/default/index.theme` and add the following: <br>
```
[icon theme] 
Inherits=cursor_theme_name
```

You may have to add a symlink to your cursor folder in your theme into the same directory as this `index.theme` file. <br>
This is the case with Polybar. <br>

## GTK-2.0

In `~/.gtkrc-2.0`, adding the following: <br>
```
gtk-cursor-theme-name=cursor_theme_name
```

## GTK-3.0

In `~/.config/gtk-3.0/settings.ini`, add the following: <br>
```
[Settings]
gtk-cursor-theme-name=cursor_theme_name
```

# Useful Tools

[xcursorgen](https://www.x.org/releases/X11R7.7/doc/man/man1/xcursorgen.1.xhtml) - Converts a .cursor and .png file(s) into an X11 cursor. <br>
[xcur2png](https://github.com/eworm-de/xcur2png) - Converts an X11 cursor file into a .cursor and .png file(s). <br>

# Licensing

Cursors used are from [Adwaita](https://gitlab.gnome.org/GNOME/adwaita-icon-theme) by the GNOME Project under [CC-BY-SA-3](https://creativecommons.org/licenses/by-sa/3.0/). <br>
Sources: <br>
- [Arch Wiki Cursor Theme Article](https://wiki.archlinux.org/title/Cursor_themes)
- [Arch Wiki xcursorgen Article](https://wiki.archlinux.org/title/Xcursorgen)
- [Gentoo Wiki Cursor Theme Article](https://wiki.gentoo.org/wiki/Cursor_themes)
- [Freedesktop.org Cursor Specifications](https://www.freedesktop.org/wiki/Specifications/cursor-spec/)