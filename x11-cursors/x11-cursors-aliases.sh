# X11 Cursor Alias Creator
# Cursors on Linux tend to have a large variety of names. This script automatically creates symlinks for most of the cursors.
# To use: place this file in the same directory as all of your X11 cursor files. Run the script.
# One day I'll update this so that any name can be used.

# Cursors

ln -s default arrow
ln -s default left_ptr
ln -s default top_left_arrow

ln -s progress left_ptr_watch
ln -s progress 08e8e1c95fe2fc01f976f1e063a24ccd
ln -s progress 3ecb610c1bf2410f44200f48c40d3599

ln -s link 3085a0e285430894940527032f8b26df
ln -s link 640fb0e74195791501fd1ed57b41487f

ln -s right_ptr draft_large
ln -s right_ptr draft_small

# General

ln -s crossed_circle not-allowed
ln -s crossed_circle 03b6e0fcb3499374a867c041f52298f0

ln -s help left_ptr_help
ln -s help question_arrow
ln -s help 5c6cd98b3f3ebcb1f9c7f1c204630408
ln -s help d9ce0ab605698f320427677b458ad60b

ln -s wait watch

# Hands

ln -s hand hand2
ln -s hand pointer
ln -s hand e29285e634086352946a0e7090d73106
ln -s hand 9d800788f1b08800ae810202380a0822

ln -s grab hand1
ln -s grab openhand

ln -s alias dnd-link

ln -s copy dnd-copy
ln -s copy 1081e37283d90000800003c07f3ef6bf
ln -s copy 6407b0e94181790501fd1e167b474872

ln -s no-drop dnd-no-drop

# Resizing and Arrows

ln -s bottom_left_corner sw-resize

ln -s bottom_right_corner se-resize

ln -s top_left_corner nw-resize

ln -s top_right_corner ne-resize

ln -s bottom_side s-resize

ln -s left_side w-resize

ln -s right_side e-resize

ln -s top_side n-resize

ln -s bd_double_arrow nwse-resize
ln -s bd_double_arrow size_fdiag
ln -s bd_double_arrow c7088f0f3e6c8088236ef8e1e3e70000

ln -s fd_double_arrow nesw-resize
ln -s fd_double_arrow size_bdiag
ln -s fd_double_arrow fcf1c3c7cd4491d801f1e1c78f100000

ln -s h_double_arrow sb_h_double_arrow
ln -s h_double_arrow col-resize
ln -s h_double_arrow ew-resize
ln -s h_double_arrow size_hor
ln -s h_double_arrow 028006030e0e7ebffc7f7070c0600140
ln -s h_double_arrow 14fef782d02440884392942c11205230

ln -s double_arrow ns-resize
ln -s double_arrow row-resize
ln -s double_arrow sb_v_double_arrow
ln -s double_arrow size_ver
ln -s double_arrow v_double_arrow
ln -s double_arrow 00008160000006810000408080010102
ln -s double_arrow 2870a09082c103050810ffdffffe0204

ln -s move 4498f0e0c1937ffe01fd06f973665830
ln -s move 9081237383d90e509aa00f00170e968f

ln -s cross cross_reverse
ln -s cross crosshair
ln -s cross diamond_cross

ln -s dotbox dot_box_mask
ln -s dotbox draped_box